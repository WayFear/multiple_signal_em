import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from scipy.optimize import brentq
from scipy.interpolate import interp1d

import numpy as np
import pickle
import collections
from pandas import DataFrame
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
import tools
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.metrics import roc_curve, auc

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# find folders
parent_dir = join(project_dir, cfg['draw']['infer']['folder'], cfg['draw']['infer']['site'])
vec_files = tools.scanfolder(parent_dir, 'vec.pk')
truth_files = tools.scanfolder(parent_dir, 'true.pk')
pred_files = tools.scanfolder(parent_dir, 'predict.pk')
model_files = tools.scanfolder(parent_dir, '.pkl')
# print(vec_files)
vec_files.sort(reverse=True)
truth_files.sort(reverse=True)
model_files.sort(reverse=True)
pred_files.sort(reverse=True)

print(truth_files)
print(vec_files)

truth_enc_ls, vec_ls, method_ls = [], [], []
for vec, truth, pred, model in zip(vec_files, truth_files, pred_files, model_files):
    tmp = truth.split('/')[-2].split('_')[0]
    if len(tmp) < 3:
        tmp = tmp.upper()
    print(tmp)
    if tmp not in cfg['draw']['infer']['methods']:
        continue

    if tmp == 'DT':
        m_name = 'D-AutoTune'
    else:
        m_name = tmp
    method_ls.append(m_name)

    # load model
    with open(model, 'rb') as infile:
        (m, class_names) = pickle.load(infile)

    le = LabelBinarizer()
    le.fit(class_names)

    # load files
    with open(vec, 'rb') as f:
        vec_dict = pickle.load(f)
    vec_dict = collections.OrderedDict(sorted(vec_dict.items(), reverse=True))
    vec_ls.append(list(vec_dict.values()))
    # print(len(vec_dict))

    with open(truth, 'rb') as f:
        truth_dict = pickle.load(f)
    truth_dict = collections.OrderedDict(sorted(truth_dict.items(), reverse=True))
    truth_ls = list(truth_dict.values())
    truth_enc_ls.append(le.transform(truth_ls))

    # print(len(truth_dict))

    with open(pred, 'rb') as f:
        pred_dict = pickle.load(f)
    pred_dict = collections.OrderedDict(sorted(pred_dict.items(), reverse=True))
    pred_ls = list(pred_dict.values())
    # print(len(pred_dict))

print(method_ls)
# compute roc and eer
fpr_dict = dict()
tpr_dict = dict()
roc_auc_dict = dict()
eer_dict = dict()

df = DataFrame(
        columns=('method', 'roc_auc', 'eer'))
for truth_enc, vec, method in zip(truth_enc_ls, vec_ls, method_ls):
    fpr_dict[method], tpr_dict[method], _ = roc_curve(np.array(truth_enc).ravel(), np.array(vec).ravel())
    roc_auc_dict[method] = auc(fpr_dict[method], tpr_dict[method])
    eer_dict[method] = brentq(lambda x: 1. - x - interp1d(fpr_dict[method], tpr_dict[method])(x), 0., 1.)
    df.loc[len(df)] = [method, roc_auc_dict[method], eer_dict[method]]
    print('Method {} has roc_auc {}, eer {}'.format(method, roc_auc_dict[method], eer_dict[method]))

# plot
lw = 16
linestyle_ls = [':', '--', '-.', '-', '-']
figsize=(22, 20)
ft_size=70
tick_size=80
label_size=90

plt.rcParams.update({'font.size': ft_size})
plt.rcParams.update({'figure.autolayout': True})
plt.rc('xtick', labelsize=tick_size)
plt.rc('ytick', labelsize=tick_size)

plt.figure(figsize=figsize)
# co
j = 0
for method in method_ls:
    fpr, tpr, roc_auc, eer = fpr_dict[method], tpr_dict[method], roc_auc_dict[method], eer_dict[method]
    plt.plot(fpr, tpr, linestyle=linestyle_ls[j],
             lw=lw, label='%s' % method)
    j += 1

# plt.plot([0, 1], [0, 1], color='darkorange', lw=2, linestyle='--')
plt.xlim([-0.01, 1.0])
plt.ylim([0.01, 1.05])
plt.xlabel('False Positive Rate', fontsize=label_size)
plt.ylabel('True Positive Rate', fontsize=label_size)
# plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")

# save fig
fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['infer']['site'])
if not os.path.exists(fig_folder):
    os.makedirs(fig_folder)
fig_path = join(fig_folder, 'infer_' + cfg['draw']['infer']['site'] +'.pdf')
plt.savefig(fig_path, bbox_inches='tight')
fig_path = join(fig_folder, 'infer_' + cfg['draw']['infer']['site'] +'.png')
plt.savefig(fig_path, bbox_inches='tight')

csv_path = join(fig_folder, 'infer_' + cfg['draw']['infer']['site']) + '.csv'
df.to_csv(csv_path)

