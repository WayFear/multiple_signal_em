import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from scipy.optimize import brentq
from scipy.interpolate import interp1d

import numpy as np
import pickle
import collections
from pandas import DataFrame
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
import tools
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.metrics import roc_curve, auc


project_dir = os.path.dirname(os.getcwd())
with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# find folders
parent_dir = join(project_dir, cfg['draw']['cmc']['folder'], cfg['draw']['cmc']['site'])
vec_files = tools.scanfolder(parent_dir, 'vec.pk')
truth_files = tools.scanfolder(parent_dir, 'true.pk')
pred_files = tools.scanfolder(parent_dir, 'predict.pk')

model_files = tools.scanfolder(parent_dir, '.pkl')
# print(vec_files)
vec_files.sort(reverse=True)
truth_files.sort(reverse=True)
model_files.sort(reverse=True)
pred_files.sort(reverse=True)

print(truth_files)
print(vec_files)

truth_enc_ls, vec_ls, method_ls = [], [], []
for vec, truth, pred, model in zip(vec_files, truth_files, pred_files, model_files):
    tmp = truth.split('/')[-2].split('_')[0]
    if len(tmp) < 3:
        tmp = tmp.upper()
    print(tmp)
    if tmp not in cfg['draw']['cmc']['methods']:
        continue

    if tmp == 'DT':
        m_name = 'D-AutoTune'
    else:
        m_name = tmp
    method_ls.append(m_name)

    # load model
    with open(model, 'rb') as infile:
        (m, class_names) = pickle.load(infile)

    le = LabelBinarizer()
    le.fit(class_names)

    # load files
    with open(vec, 'rb') as f:
        vec_dict = pickle.load(f)
    vec_dict = collections.OrderedDict(sorted(vec_dict.items(), reverse=True))
    vec_ls.append(list(vec_dict.values()))
    # print(len(vec_dict))

    with open(truth, 'rb') as f:
        truth_dict = pickle.load(f)
    truth_dict = collections.OrderedDict(sorted(truth_dict.items(), reverse=True))
    truth_ls = list(truth_dict.values())
    truth_enc_ls.append(le.transform(truth_ls))

print(method_ls)
# compute cmc
t_ls = []
for l in truth_enc_ls:
    t_ls.append(np.argmax(np.array(l), axis=1))

n_guesses = len(le.classes_) + 1
cmc_dict = {}
df = DataFrame(
        columns=('method', 'accuracy'))
for t, v, method in zip(t_ls, vec_ls, method_ls):
    acc_ls = []
    for i in range(1, n_guesses):
        refined_acc = tools.top_k_guesses(v, t, i)
        acc_ls.append(refined_acc)
    cmc_dict[method] = acc_ls
    print('Method {} has acc ls {}'.format(method, acc_ls))
    df.loc[len(df)] = [method, acc_ls[0]]

# plot
lw = 16
linestyle_ls = [':', '--', '-.', '-', '-']
figsize=(22, 20)
ft_size=70
tick_size=80
label_size=90

plt.rcParams.update({'font.size': ft_size})
plt.rcParams.update({'figure.autolayout': True})
plt.rc('xtick', labelsize=tick_size)
plt.rc('ytick', labelsize=tick_size)

plt.figure(figsize=figsize)
# co
j = 0
for method in method_ls:
    cmc = cmc_dict[method]
    # cmc = [a * 100 for a in cmc]
    plt.plot(range(1, n_guesses, 1), cmc, linestyle=linestyle_ls[j],
             lw=lw, label='%s' % method)
    j += 1

plt.xlim([0.5, n_guesses])
plt.ylim([0.01, 1.05])
plt.xlabel('Rank', fontsize=label_size)
plt.ylabel('Identification Accuracy', fontsize=label_size)
# plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")

# save fig
fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['cmc']['site'])
if not os.path.exists(fig_folder):
    os.makedirs(fig_folder)
fig_path = join(fig_folder, 'cmc_' + cfg['draw']['cmc']['site'] +'.pdf')
plt.savefig(fig_path, bbox_inches='tight')
fig_path = join(fig_folder, 'cmc_' + cfg['draw']['cmc']['site'] +'.png')
plt.savefig(fig_path, bbox_inches='tight')

csv_path = join(fig_folder, 'cmc_' + cfg['draw']['cmc']['site']) + '.csv'
df.to_csv(csv_path)
