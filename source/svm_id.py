import yaml
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from os.path import join
import tensorflow as tf
import numpy as np
from pandas import DataFrame
import facenet
import os
import sys
import math
import pickle
from sklearn.svm import SVC
import tools

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

if cfg['specs']['set_gpu']:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(cfg['base_conf']['gpu_num'])

batch_size = 1000
nrof_train_images_per_class=100
min_nrof_images_per_class=10
image_size=160
threshold = 0.6

kernel = cfg['svm_inference']['kernel']
model_name = cfg['svm_inference']['model_name']
path_train = cfg['svm_inference']['path_train']
test = cfg['svm_inference']['test']
test_cameras = cfg['svm_inference']['test_cameras']
path_other = cfg['svm_inference']['path_other']
folder = cfg['svm_inference']['folder']
mode = cfg['svm_inference']['mode']

project_dir = os.path.dirname(os.getcwd())


def embedding(path):
    
    with tf.Graph().as_default():

        with tf.Session() as sess:

            np.random.seed(seed=2)

            dataset = facenet.get_dataset(path)
            dataset = facenet.get_dataset_excluother(path, 'other')

            paths, labels = facenet.get_image_paths_and_labels(dataset)
            print(labels)
            print('Number of classes: %d' % len(dataset))
            print('Number of images: %d' % len(paths))

            # Load the model
            print('Loading feature extraction model...')
            model_path = join(project_dir, folder, '%s.pb'%model_name)
            facenet.load_model(model_path)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            embedding_size = embeddings.get_shape()[1]

            # Run forward pass to calculate embeddings
            print('Calculating features for images...')
            nrof_images = len(paths)
            nrof_batches_per_epoch = int(math.ceil(1.0 * nrof_images / batch_size))
            emb_array = np.zeros((nrof_images, embedding_size))
            
            for i in range(nrof_batches_per_epoch):
                start_index = i * batch_size
                end_index = min((i + 1) * batch_size, nrof_images)
                paths_batch = paths[start_index:end_index]
                images = facenet.load_data(paths_batch, False, False, image_size)
                feed_dict = {images_placeholder: images, phase_train_placeholder: False}
                emb_array[start_index:end_index, :] = sess.run(embeddings, feed_dict=feed_dict)
    
    return paths, labels, emb_array, dataset


df = DataFrame(
        columns=('camera', 'accuracy'))
cross_id_dict = {}
for test_camera in test_cameras:
    # Get the embedding results of train dataset
    if mode == "train":
        data_dir = join(project_dir, folder, path_train)
        paths, labels, emb_array, dataset = embedding(data_dir)

        # Use embedding results to train SVM classifiers
        print('Training classifier...')
        model = SVC(kernel=kernel, probability=True, random_state=666)
        model.fit(emb_array, labels)

        class_names = [cls.name.replace('_', ' ') for cls in dataset]
        # save model
        pickle.dump([model, class_names], open(join(project_dir, folder, 'id_auto_tune.pkl'), 'wb'))
    else:
        with open(join(project_dir, folder, 'id_auto_tune.pkl'), 'rb') as infile:
            (model, class_names) = pickle.load(infile)

    # Get the embedding results of test dataset
    data_dir = join(test, test_camera)
    paths, labels, emb_array, dataset = embedding(data_dir)

    name_labels =[]
    for path in paths:
        name = path.split('/')[-2]
        name_labels.append(name)

    # Use SVW classifiers to classify testing embedding results
    print('Testing classifier...')
    predictions = model.predict_proba(emb_array)

    acc_ls = []
    n_guesses = len(class_names)
    for i in range(1, n_guesses):
        refined_acc = tools.top_k_guesses_str(predictions, name_labels, class_names, i)
        acc_ls.append(refined_acc)
    cross_id_dict[test_camera] = acc_ls
    print('Camera {} has acc ls {}'.format(test_camera, acc_ls))
    df.loc[len(df)] = [test_camera, acc_ls[0]]

# plot
lw = 16
linestyle_ls = [':', '--', '-.', '-', '-']
figsize=(22, 20)
ft_size=70
tick_size=80
label_size=90

plt.rcParams.update({'font.size': ft_size})
plt.rcParams.update({'figure.autolayout': True})
plt.rc('xtick', labelsize=tick_size)
plt.rc('ytick', labelsize=tick_size)

plt.figure(figsize=figsize)
# co
j = 0
for test_camera in test_cameras:
    cmc = cross_id_dict[test_camera]
    # cmc = [a * 100 for a in cmc]
    plt.plot(range(1, n_guesses, 1), cmc, linestyle=linestyle_ls[j],
             lw=lw, label='%s' % cfg['svm_inference']['camera_map'][test_camera])
    j += 1

plt.xlim([0.5, n_guesses])
plt.ylim([0.01, 1.05])
plt.xlabel('Rank', fontsize=label_size)
plt.ylabel('Identification Accuracy', fontsize=label_size)
# plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")

# save fig
fig_folder = join(cfg['base_conf']['fig_base'], cfg['svm_inference']['site'])
if not os.path.exists(fig_folder):
    os.makedirs(fig_folder)
fig_path = join(fig_folder, 'cross_id_' + cfg['svm_inference']['site'] +'.pdf')
plt.savefig(fig_path, bbox_inches='tight')
fig_path = join(fig_folder, 'cross_id_' + cfg['svm_inference']['site'] +'.png')
plt.savefig(fig_path, bbox_inches='tight')

csv_path = join(fig_folder, 'cross_id_' + cfg['svm_inference']['site']) + '.csv'
df.to_csv(csv_path)





# best_class_indices = np.argmax(predictions, axis=1)
# print(best_class_indices)
# best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
# resu_save = []
# save_label = []
# 
# for i in range(len(best_class_indices)):
#     resu_save.append(class_names[best_class_indices[i]])
#     save_label.append(name_labels[i])
#     print('%4d  %s: %.3f %s' % (i, class_names[best_class_indices[i]], best_class_probabilities[i], name_labels[i]))
#     
# # accuracy = np.mean(np.equal(best_class_indices, labels))
# a = np.array(resu_save)
# b = np.array(name_labels)
# accuracy = np.sum(a == b)/float(len(resu_save))
# print('Accuracy: %.3f' % accuracy)
# 
# print('{} POI: {}'.format(len(class_names), class_names))
# 
# 
# paths = [os.path.split(p)[1] for p in paths]
# 
# 
# # In[34]:
# 
# 
# paths = [p.split('.')[0] for p in paths]
# 
# 
# with open('pre_612', 'wb') as outfile:
#     pickle.dump(dict(zip(paths, resu_save)), outfile)
# with open('label', 'wb') as outfile:
#     pickle.dump(dict(zip(paths, save_label)), outfile)
# 
# 
# # In[39]:
# 
# 
# dict(zip(paths, resu_save))
# 
# 
# # In[37]:
# 
# 
# labels
# 
# 
# # In[ ]:
# 
# 
# 
# 
