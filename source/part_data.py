import os
from os.path import join
import numpy as np
import re
from sklearn.cluster import DBSCAN, AgglomerativeClustering
import pickle
import tools
import argparse
import sys
import csv
from shutil import copytree, rmtree, copyfile
import random
import math


def main(args):
    project_dir = os.path.dirname(os.getcwd())
    data_dir = join(project_dir, args.data_name)
    meeting_path = os.listdir(data_dir)
    meeting_path = list(filter(lambda x: x!='true_label.pk', meeting_path))
    all_num = len(meeting_path)
    meet_num = all_num*args.rate
    random.seed(2)
    selected_meet = random.sample(meeting_path, math.ceil(meet_num))
    data_floder = join(project_dir, '%s_%1.1f'%(args.data_name, args.rate))
    if os.path.exists(data_floder):
        rmtree(data_floder)
    os.mkdir(data_floder)
    for meet in selected_meet:
        os.mkdir(join(data_floder, meet))
        paths = os.listdir(join(data_dir, meet))
        copytree(join(data_dir, meet, 'mtcnn'), join(data_floder, meet, 'mtcnn'))
        paths = list(filter(lambda x: re.match(r'.+\.png$', x), paths))
        copyfile(join(data_dir, meet, paths[0]), join(data_floder, meet, paths[0]))
    copyfile(join(data_dir, 'true_label.pk'), join(data_floder, 'true_label.pk'))


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data_name', type=str,
                        help='The start of add people num.', default='middle_data')
    parser.add_argument('-r', '--rate', type=float,
                        help='The end of add people num.', default=0.1)
    return parser.parse_args(argv)


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
    tools.email_subject(os.path.basename(__file__))

