import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import string
import pickle
import glob
import numpy as np
from sklearn.metrics import confusion_matrix
import tools

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# load true labels from pickle
with open(join(project_dir, cfg['draw']['conf_mat']['truth_folder'], 'true_label.pk'), 'rb') as f:
    raw_truth = pickle.load(f)

truth = {}
for k, v in raw_truth.items():
    temp = k.split('_')
    truth["%s_%s_%s"%(temp[0], temp[1], temp[2])] = v

print(len(truth))
# load predictions from csv
pred_file = join(project_dir, cfg['draw']['conf_mat']['pred_folder'], cfg['draw']['conf_mat']['pred_file'])
for csv_path in glob.glob(pred_file + '*_voting.csv'):
    print(csv_path)

pred = {}
with open(csv_path, "r") as filestream:
        for line in filestream:
            currentline = line.rstrip().split(",")
            temp = currentline[0].split('/')[-1].split('_')
            pred["%s_%s_%s" % (temp[0], temp[1], temp[2])] = currentline[1]

print(len(pred))

# extract the list of keys
keys_truth = set(truth.keys())
keys_pred = set(pred.keys())
intersection = keys_truth & keys_pred
print(len(intersection))
truth = {k: truth.get(k, None) for k in intersection}
pred = {k: pred.get(k, None) for k in intersection}

names = list(set().union(truth.values(), pred.values()))
# names = list(set(names))
idx = 0
name_2_id = {}
for name in names:
    if name.startswith('other'):
        name_2_id[name] = 'Others'
    else:
        name_2_id[name] = string.ascii_lowercase[idx]
        idx += 1

labels = list(set(list(name_2_id.values())))
labels.sort()
print(labels)

y_true = [name_2_id[i] for i in list(truth.values())]
y_pred = [name_2_id[i] for i in list(pred.values())]
# print(set(y_pred))


# Compute confusion matrix
cnf_matrix = confusion_matrix(y_true, y_pred)
np.set_printoptions(precision=1)

# Plot normalized confusion matrix
fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['conf_mat']['save_folder'])
if not os.path.exists(fig_folder):
    os.makedirs(fig_folder)

fig_path = join(fig_folder, 'cm-' + cfg['draw']['conf_mat']['pred_file'] + '.pdf')
tools.plot_confusion_matrix(cnf_matrix, classes=labels, filename=fig_path,
                            normalize=True, cmap=plt.cm.YlGn, tick_size=40, label_size=50)

