#!/bin/bash
PS4="#:"
set -x
dimension='512'
model_name='20180402-114759.pb'
para=5
sta_num=30
en_num=90
start_num=30
end_num=90
python clear_pk.py
#python generate_train_data.py --source_data_dir god_train --data_dir god_train

python fine_tune.py --epoch_size 100 --max_nrof_epochs 50 --data_dir god_train --center_loss_factor 0.5 --embedding_size $dimension --keep_probability 0.7
python freeze_graph.py --model_dir god_train_center_loss_factor_0.50  --output_file god_train.pb
python feature_extraction_classifier.py --model_dir god_train.pb --dimension_num $dimension
for i in {0..5}
do
    now=$(date '+%Y-%m-%d-%H-%M-%S')
   
    python match.py --copy_pic 0 --cycle_num $i --start_num $start_num --end_num $end_num --header $now --dimension_num $dimension --hyper_para $para
    python voting.py --start_num $sta_num --end_num $en_num --header $now
    python evaluate_by_global.py --load_label 1 --header $now
    python occ_update.py --load_label 1 --err_rate 0.05 --cycle_num $((i+1)) --center_file $now\_voting_center.pk
    let para-=1
done
