# coding: utf-8

# In[10]:


import subprocess
from os.path import join
import os
import re


folder = '/home/chris/Dropbox/video_scan/codebase/vscan_backend/eval/hyper/oxford/'
sub_folder = 'fuzzier_3'
paths = os.listdir(join(folder, sub_folder))
paths = [p for p in paths if p.endswith('report.csv')]

postfix = '.pb'
for path in paths:
    path = path.split('_')[0]
    format = '%s*'+ postfix
    file_name = format%path
    print(file_name)
    proc = subprocess.Popen(['scp', '-o BatchMode=yes',
                                    'xxlu@137.205.115.176:/home/data1/kan/oxford_1/multiple_signal_em/models/%s'%file_name,
                                    join(folder, sub_folder)],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    proc.wait()
    print('result: %s' % repr(proc.stderr.readline()))
