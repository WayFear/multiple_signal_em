import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from scipy.optimize import brentq
from scipy.interpolate import interp1d

import numpy as np
import pickle
import collections
from pandas import DataFrame
from sklearn.metrics import confusion_matrix
import tools
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.metrics import roc_curve, auc

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# find folders
parent_dir = join(project_dir, cfg['draw']['cm_infer']['folder'], cfg['draw']['cm_infer']['site'])
vec_files = tools.scanfolder(parent_dir, 'vec.pk')
truth_files = tools.scanfolder(parent_dir, 'true.pk')
pred_files = tools.scanfolder(parent_dir, 'predict.pk')

model_files = tools.scanfolder(parent_dir, '.pkl')
# print(vec_files)
vec_files.sort(reverse=True)
truth_files.sort(reverse=True)
model_files.sort(reverse=True)
pred_files.sort(reverse=True)

print(truth_files)
print(vec_files)

truth_enc_ls, method_ls, pred_enc_ls = [], [], []
for vec, truth, pred, model in zip(vec_files, truth_files, pred_files, model_files):
    tmp = truth.split('/')[-2].split('_')[0]
    if len(tmp) < 3:
        tmp = tmp.upper()
    print(tmp)
    if tmp not in cfg['draw']['cm_infer']['methods']:
        continue
    method_ls.append(tmp)

    # load model
    with open(model, 'rb') as infile:
        (m, class_names) = pickle.load(infile)

    with open(truth, 'rb') as f:
        truth_dict = pickle.load(f)
    truth_dict = collections.OrderedDict(sorted(truth_dict.items(), reverse=True))
    truth_ls = list(truth_dict.values())
    truth_enc_ls.append(truth_ls)

    # print(len(truth_dict))

    with open(pred, 'rb') as f:
        pred_dict = pickle.load(f)
    pred_dict = collections.OrderedDict(sorted(pred_dict.items(), reverse=True))
    pred_ls = list(pred_dict.values())
    pred_enc_ls.append(pred_ls)
    # print(len(pred_dict))

print(method_ls)
print(class_names)
le = LabelEncoder()
new_cls_name = le.fit_transform(class_names)


df = DataFrame(
        columns=('method', 'roc_auc', 'eer'))


for truth_enc, pred_enc, method in zip(truth_enc_ls, pred_enc_ls, method_ls):
    cnf_matrix = confusion_matrix(truth_enc, pred_enc)
    np.set_printoptions(precision=1)

    # Plot normalized confusion matrix
    fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['cm_infer']['site'])
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)

    ft_size = 40/20*len(class_names)

    fig_path = join(fig_folder, 'cm_' + cfg['draw']['cm_infer']['site'] + '_' + method + '.png')
    tools.plot_confusion_matrix(cnf_matrix, classes=new_cls_name, filename=fig_path,
                                normalize=True, cmap=plt.cm.RdPu, ft_size=ft_size, tick_size=40, label_size=50)
    #
    fig_path = join(fig_folder, 'cm_' + cfg['draw']['cm_infer']['site'] + '_' + method + '.pdf')
    tools.plot_confusion_matrix(cnf_matrix, classes=new_cls_name, filename=fig_path,
                                normalize=True, cmap=plt.cm.RdPu, ft_size=ft_size, tick_size=40, label_size=50)
