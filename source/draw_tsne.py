import os
import yaml
from os.path import join
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder
import tensorflow as tf
import facenet
import os
import cv2
import numpy as np
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('fivethirtyeight')
import pandas as pd
from os.path import join
from collections import OrderedDict
import re
import csv
from scipy.spatial import ConvexHull

project_dir = os.path.dirname(os.getcwd())
with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = str(cfg['base_conf']['gpu_num'])

data_base = cfg['base_conf']['data_base']
data_path = str(cfg['draw']['tsne']['site'])
peoples = [o for o in os.listdir(join(data_base, data_path))
                    if os.path.isdir(os.path.join(data_base, data_path, o))]
pics = {}
num_classes = len(peoples)
print('Have {} people'.format(num_classes))
sel_Subject = range(1, num_classes, int(np.ceil(num_classes * 0.1)))

# peoples = [peoples[idx] for idx in sel_Subject]
peoples = peoples[1:7]
num_to_show = cfg['draw']['tsne']['num_to_show']
for p in peoples:
    tmp = list(map(lambda x: join(data_base, data_path, p, x), os.listdir(join(data_base, data_path, p))))
    sel_length = min(num_to_show, len(tmp))
    pics[p] = tmp[:sel_length]

# In[82]:
model_names = [f for f in os.listdir(join(project_dir, cfg['draw']['tsne']['model_base'])) if f.endswith('.pb')]
model_names.sort()

result_dict = {}
std_dict = {}


# In[91]:
def get_marker(p_mk, peo):
    if peo in p_mk:
        return p_mk[peo]
    return 'X'
filled_markers = ('o', 'v', 'P', 's', '*', 'd', 'H', 'D', 'd', 'p', 'X')


peo_mak = dict(zip(peoples, filled_markers))
def get_label(name, label):
    r = name.split('/')
    temp = r[-1].split('_')
    temp = '%s_%s_%s' % (temp[0], temp[1], temp[2])
    if temp in label:
        return label[temp]
    return None

for model_name in model_names:
    # -----------------------------
    # step1: load features
    # -----------------------------
    csv_name = model_name.split('.')[0]
    csv_name = [f for f in os.listdir(join(project_dir, cfg['draw']['tsne']['model_base']))
                   if re.match(re.escape(csv_name)+r'.+voting\.csv$', f)][0]
    predict_label = {}
    
    with open(join(project_dir, cfg['draw']['tsne']['model_base'], csv_name)) as f:
        result = csv.reader(f, delimiter=',')
        for row in result:
            r = row[0].split('/')
            temp = r[-1].split('_')
            r[-1] = '%s_%s_%s' % (temp[0], temp[1], temp[2])
            predict_label[r[-1]] = row[1]
#     pre_label = [get_label(i, predict_label) for i in pics]
    vectors = []
    labels = []
    cluster_len = []
    result_dict[model_name] = {}
    std_dict[model_name] = {}
    with tf.Graph().as_default():
        config = tf.ConfigProto()
        config.gpu_options.visible_device_list = str(cfg['base_conf']['gpu_num'])
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.7)
        with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
            facenet.load_model(join(project_dir, cfg['draw']['tsne']['model_base'], model_name))
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
    p_idx = 0
    emd_dim = int(embeddings.get_shape()[1])
    pre_label = []
    name_num = {}
    for p in pics:
        image_list = []

        data_result_dict = np.empty([0, emd_dim])

        pics[p].sort()
        for pic_path in pics[p]:
            im = cv2.imread(pic_path)
            pre_la = get_label(pic_path, predict_label)
            if pre_la not in peoples:
                continue
            if pre_la is None:
                continue
            pre_label.append(pre_la)
            # print(pic_path)
            prewhitened = facenet.prewhiten(im)
            image_list.append(prewhitened)
            if len(image_list) == 1000:
                images = np.stack(image_list)
                feed_dict = {images_placeholder: images, phase_train_placeholder: False}
                emb = sess.run(embeddings, feed_dict=feed_dict)

                data_result_dict = np.vstack((data_result_dict, emb))
                print(emb.shape)
                del image_list[:]
        if len(image_list) != 0:
            images = np.stack(image_list)
            feed_dict = {images_placeholder: images, phase_train_placeholder: False}
            emb = sess.run(embeddings, feed_dict=feed_dict)
            data_result_dict = np.vstack((data_result_dict, emb))

        mean = np.mean(data_result_dict, axis=0)
        std = np.std(data_result_dict, axis=0)
        result_dict[model_name][p] = mean
        std_dict[model_name][p] = np.average(std)
        print("%s: "% p)
        print("mean: ", np.linalg.norm(mean))
        print("std: ", np.average(std))
        print("\n")
        vectors += data_result_dict.tolist()
        labels += [p_idx]*data_result_dict.shape[0]
        name_num[p] = p_idx
        p_idx += 1
        cluster_len.append(data_result_dict.shape[0])
        
   

    # -----------------------------
    # step2: t-sne
    # -----------------------------
    features = np.array(vectors)
    print('...to plot...')
    fig_dir = join(cfg['base_conf']['fig_base'], 'tsne', str(cfg['draw']['tsne']['perplexity']))
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)

    feat_cols = ['fea%s'%str(i) for i in range(features.shape[1])]
    df = pd.DataFrame(features, columns=feat_cols)
    df['Subject'] = np.array(labels)
    df['Pre_label'] = np.array(pre_label)

    # select certain Subjects
    num_classes = len(peoples)
    sel_Subject = range(1, num_classes, int(np.ceil(num_classes * 0.1)))
    sel_Subject = range(0, num_classes)
    df = df.loc[df['Subject'].isin(sel_Subject)]

    le = LabelEncoder()
    le.fit(df['Subject'])
    # print label as str
    df['Subject'] = df['Subject'].apply(lambda i: str(i))

    perplexity = cfg['draw']['tsne']['perplexity']
    tsne = TSNE(n_components=2, verbose=1, perplexity=perplexity, n_iter=1000, random_state=123)
    tsne_results = tsne.fit_transform(df[feat_cols].values)

    df['x-tsne'] = tsne_results[:, 0]
    df['y-tsne'] = tsne_results[:, 1]

    # -----------------------------
    # step3: save figs and csvs
    # -----------------------------
    fig, ax = plt.subplots()
    # fig.set_size_inches(20, 20)
    marker_size = 12

    # for i in le.classes_:
    #     # print(df.iloc[0]['Subject'])
    #     # print(type(df.iloc[0]['Subject']))
    #     r = df[df['Subject'] == str(i)]
    #     print('selected df is {}'.format(r))
    #     tmp = le.transform([i])
    #     for x, y, m in zip(r['x-tsne'], r['y-tsne'], r['Pre_label']):
    #         ax.scatter(x, y, marker=get_marker(peo_mak, m), s=marker_size, c='C'+str(tmp[0]), label=str(tmp[0]))
    #

    peo_points = []
    for i, r in df.iterrows():
        tmp = le.transform([r['Subject']])
        truth_id = r['Subject']
        pre_id = str(name_num[r['Pre_label']])
        if pre_id == truth_id:
            tmp_lab = 'true_' + truth_id
        else:
            tmp_lab = 'false_' + pre_id

        ax.plot(r['x-tsne'], r['y-tsne'], marker=get_marker(peo_mak, r['Pre_label']), markersize=marker_size,
                color='C'+str(tmp[0]), alpha=0.7, label=tmp_lab, linestyle='None')
        peo_points.append([r['x-tsne'], r['y-tsne']])

    peo_points = np.array(peo_points)
    hull = ConvexHull(peo_points)
    ax.plot(peo_points[hull.vertices, 0], peo_points[hull.vertices, 1], color='black', marker='o', lw=4)
    last_line = [hull.vertices[0], hull.vertices[-1]]
    ax.plot(peo_points[last_line, 0], peo_points[last_line, 1], color='black', marker='o', lw=4)

    ax.set_xlim([-22, 20])
    ax.set_ylim([-15, 18])
    handles, labels = ax.get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    plt.legend(handle_list, label_list, bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3, fontsize=17)
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
    #            ncol=3)
    name = model_name.split('.')[0]
    plt.savefig(fig_dir + '/' + name + '_tnse.png', bbox_inches='tight')
    plt.savefig(fig_dir + '/' + name + '_tnse.pdf', bbox_inches='tight')

    csv_path = join(fig_dir, name) + '.csv1'
    header = ['x-tsne', 'y-tsne', 'Subject']
    df.to_csv(csv_path, columns=header)
    
    # fig, ax = plt.subplots()
    # peo_points = np.array(peo_points)
    # ax.plot(peo_points[:,0], peo_points[:,1], 'o', markersize=marker_size,
    #             color='C'+str(1), alpha=0.7, linestyle='None')

    # ax.set_xlim([-20, 20])
    # ax.set_ylim([-15, 15])
    # plt.savefig(fig_dir + '/' + name + '_hull.png', bbox_inches='tight')
    # plt.savefig(fig_dir + '/' + name + '_hull.pdf', bbox_inches='tight')


