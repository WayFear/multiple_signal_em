import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
import matplotlib.ticker as ticker
import tools
import re
import numpy as np
import csv
import pickle
import collections
from pandas import DataFrame
from sklearn.metrics import precision_recall_fscore_support, accuracy_score

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

color_sink = [('C2', 'C3')] * 3
# color_sink = [ ('C0', 'C1'), ('C2', 'C3'), ('C4', 'C5')]
# color_sink = [('C4', 'C5'), ('C4', 'C5'), ('C4', 'C5')]
method_idx = 0
for method in cfg['draw']['errors']['methods']:
    df = tools.load_pie_report_v2(project_dir, cfg, method)
    # plot
    fig, ax1 = plt.subplots(figsize=(23, 10))
    plt.locator_params(axis='x', nticks=5)
    tick_size = 55
    label_size = 75

    lw = 12
    marker_size = 50
    
    color_idx = cfg['draw']['errors']['err_type'] - 1
    print(color_sink[color_idx])
    t = df['ctrl_var'].tolist()
    ax1.plot(df['ctrl_var'].tolist(), df['F1 score'].tolist(), color_sink[color_idx][0] + 'o-',
             linewidth=lw, markersize=marker_size)
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(np.mean(np.diff(t))))
    ax1.set_xlabel(cfg['draw']['errors']['err_map'][cfg['draw']['errors']['err_type']], fontsize=label_size)
    ax1.tick_params('x', labelsize=tick_size)
    # Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylim([0.5, 1])
    ax1.set_ylabel(df.columns[-3], color=color_sink[color_idx][0], fontsize=label_size)
    ax1.tick_params('y', colors=color_sink[color_idx][0], labelsize=tick_size)

    ax2 = ax1.twinx()
    ax2.plot(df['ctrl_var'].tolist(), df['converge_iter.'].tolist(), color_sink[color_idx][1] + 'd-.',
             linewidth=lw, markersize=marker_size)
    ax2.set_ylabel(df.columns[-1], color=color_sink[color_idx][1], fontsize=label_size)
    ax2.tick_params('y', colors=color_sink[color_idx][1], labelsize=tick_size)
    ax2.set_ylim([2, 12])
    method_idx += 1

    # save fig
    fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['errors']['site'])
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)
    fig_path = join(fig_folder, 'errors_' + method + '_' + str(cfg['draw']['errors']['err_type']) + '.pdf')
    fig.savefig(fig_path, bbox_inches='tight')
    fig_path = join(fig_folder, 'errors_' + method + '_' + str(cfg['draw']['errors']['err_type']) + '.png')
    fig.savefig(fig_path, bbox_inches='tight')

    csv_path = join(fig_folder, 'errors_' + str(cfg['draw']['errors']['err_type']) + '_' + cfg['draw']['errors']['site']) + '.csv'
    df.to_csv(csv_path)
