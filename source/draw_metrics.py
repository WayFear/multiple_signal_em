import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
import pandas as pd
import re

import pickle
import collections
from pandas import DataFrame
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
import tools

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# load true labels from pickle
with open(join(project_dir, cfg['draw']['metrics']['truth_folder'], cfg['draw']['metrics']['site'], 'true_label.pk'), 'rb') as f:
    raw_truth = pickle.load(f)

# load predictions from csv
pred_dir = join(project_dir, cfg['draw']['metrics']['pred_folder'], cfg['draw']['metrics']['site'])
filenames = os.listdir(pred_dir)

# explore the results to see the limits
if cfg['draw']['metrics']['exploration']:
    filenames = [filename for filename in filenames if filename.endswith('voting.csv1')]

    filenames.sort()
    for filename in filenames:
        truth = {}
        for k, v in raw_truth.items():
            temp = k.split('_')
            truth["%s_%s_%s" % (temp[0], temp[1], temp[2])] = v

        len_truth = len(truth)

        csv_path = join(pred_dir, filename)
        pred = {}
        i = 0
        with open(csv_path, "r") as filestream:
                for line in filestream:
                    i += 1
                    currentline = line.rstrip().split(",")
                    temp = currentline[0].split('/')[-1].split('_')
                    pred["%s_%s_%s" % (temp[0], temp[1], temp[2])] = currentline[1]

        len_pred = len(pred)
        # print('Size of predictions: {}, num of lines is {}'.format(len(pred), i))

        # extract the list of keys
        keys_truth = set(truth.keys())
        keys_pred = set(pred.keys())
        intersection = keys_truth & keys_pred
        # print('Size of intersection: {}'.format(len(intersection)))
        truth = {k: truth.get(k, None) for k in intersection}
        pred = {k: pred.get(k, None) for k in intersection}

        names = list(set().union(truth.values(), pred.values()))

        y_true = list(truth.values())
        y_pred = list(pred.values())

        # Compute confusion matrix
        report_lr = precision_recall_fscore_support(y_true, y_pred, average='macro')
        acc = accuracy_score(y_true, y_pred, normalize=True)
        print("File %s: num_truth= %d, num_pred = %d, intersection = %d, precision = %0.3f, recall = %0.3f, F1 = %0.3f, accuracy = %0.3f\n" % \
                   (csv_path.split('/')[-1], len_truth, len_pred, len(intersection), report_lr[0], report_lr[1], report_lr[2], acc))


if cfg['draw']['metrics']['compare']:
    site = cfg['draw']['metrics']['site']
    methods_dict = cfg['draw']['metrics']['methods'][site]
    methods_dict = collections.OrderedDict(sorted(methods_dict.items(), reverse=True))
    method_files = list(methods_dict.values())
    eff_files = list()
    for method in method_files:
        for filename in filenames:
            if method in filename:
                eff_files.append(filename)

    print(eff_files)

    df = DataFrame(
        columns=(' ', 'precision', 'recall', 'f1 score', 'accuracy'))
    j = 0
    for filename in eff_files:
        truth = {}
        for k, v in raw_truth.items():
            temp = k.split('_')
            truth["%s_%s_%s" % (temp[0], temp[1], temp[2])] = v

        len_truth = len(truth)

        csv_path = join(pred_dir, filename)
        pred = {}
        i = 0
        with open(csv_path, "r") as filestream:
                for line in filestream:
                    i += 1
                    currentline = line.rstrip().split(",")
                    temp = currentline[0].split('/')[-1].split('_')
                    pred["%s_%s_%s" % (temp[0], temp[1], temp[2])] = currentline[1]

        len_pred = len(pred)
        # print('Size of predictions: {}, num of lines is {}'.format(len(pred), i))

        # extract the list of keys
        keys_truth = set(truth.keys())
        keys_pred = set(pred.keys())
        intersection = keys_truth & keys_pred
        # print('Size of intersection: {}'.format(len(intersection)))
        truth = {k: truth.get(k, None) for k in intersection}
        pred = {k: pred.get(k, None) for k in intersection}

        names = list(set().union(truth.values(), pred.values()))

        y_true = list(truth.values())
        y_pred = list(pred.values())

        # Compute metrics
        report_lr = precision_recall_fscore_support(y_true, y_pred, average='macro')
        acc = accuracy_score(y_true, y_pred, normalize=True)

        print("File %s: num_truth= %d, num_pred = %d, intersection = %d, precision = %0.3f, recall = %0.3f, F1 = %0.3f, accuracy = %0.3f\n" % \
                   (list(methods_dict.keys())[j], len_truth, len_pred, len(intersection), report_lr[0], report_lr[1], report_lr[2], acc))

        if list(methods_dict.keys())[j] == 'DT':
            m_name = 'D-AutoTune'
        else:
            m_name = list(methods_dict.keys())[j]

        df.loc[len(df)] = [m_name, report_lr[0], report_lr[1], report_lr[2], acc]

        j += 1

    df = df.set_index(' ').T
    print(df)

    # save to fig dir
    # plot = df.plot.bar(alpha=0.75, rot=0, figsize=(23, 12), fontsize=50, width=0.8, linewidth=4, edgecolor=['black']*df.shape[1])
    plot = df.plot.bar(alpha=0.75, rot=0, figsize=(23, 12), fontsize=50, width=0.8)
    plot.set_ylim([0, 1.19])
    plot.set_ylabel("Performance", fontsize=60)
    ncol = df.shape[1]
    print(df.shape)
    l_ft = 40*4/df.shape[1]
    plot.legend(ncol=ncol, loc=9, fontsize=l_ft)
    fig = plot.get_figure()
    fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['metrics']['site'])
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)
    fig_path = join(fig_folder, 'assoc_' + cfg['draw']['metrics']['site'] + '.pdf')
    fig.savefig(fig_path, bbox_inches='tight')
    fig_path = join(fig_folder, 'assoc_' + cfg['draw']['metrics']['site'] + '.png')
    fig.savefig(fig_path, bbox_inches='tight')

    csv_path = join(fig_folder, 'assoc_' + cfg['draw']['metrics']['site']) + '.csv'
    df.to_csv(csv_path)