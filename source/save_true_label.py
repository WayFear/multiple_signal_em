import os
import yaml
from os.path import join
from datetime import datetime
import pickle
from shutil import copyfile


project_dir = os.path.dirname(os.getcwd())
with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

middle_data_path = join(project_dir, cfg['base_conf']['middle_data_path'])
meeting_paths = os.listdir(middle_data_path)
meeting_paths = list(filter(lambda x: x != 'time' and x != 'tmp' and x != 'problem data', meeting_paths))

true_label = {}
for meet in meeting_paths:
    name_paths = os.listdir(join(middle_data_path, meet, 'classifier'))
    for name in name_paths:
        if os.path.isfile(join(middle_data_path, meet, 'classifier',name)):
            continue
        for pic in os.listdir(join(middle_data_path, meet, 'classifier', name)):
            true_label[pic] = name

if os.path.isfile(join(project_dir, 'final_result', 'true_label.pk')):
    time = datetime.strftime(datetime.now(), '%m-%d-%H-%M-%S')
    copyfile(join(project_dir, 'final_result', 'true_label.pk'),
             join(project_dir, 'important_file', '%s_true_label.pk'%time))
with open(join(project_dir, 'final_result', 'true_label.pk'), 'wb') as f:
    pickle.dump(true_label, f, protocol=pickle.HIGHEST_PROTOCOL)