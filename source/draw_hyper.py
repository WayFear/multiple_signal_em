import os
import yaml
from os.path import join
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('seaborn')
import re
import numpy as np

import pickle
import matplotlib.ticker as ticker
from pandas import DataFrame
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
import tools

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

# load true labels from pickle
with open(join(project_dir, cfg['draw']['hyper']['truth_folder'], cfg['draw']['hyper']['site'], 'true_label.pk'), 'rb') as f:
    raw_truth = pickle.load(f)

# load predictions from csv
pred_dir = join(project_dir, cfg['draw']['hyper']['pred_folder'], cfg['draw']['hyper']['site'])
filenames = os.listdir(pred_dir)
doi = [f for f in filenames if f.startswith(cfg['draw']['hyper']['para'])]
print(doi)

doi.sort()
df = DataFrame(
            columns=(' ', 'precision', 'recall', 'F1 score', 'accuracy', 'converge_iter.'))

for d in doi:
    filenames = list(filter(lambda x: re.match(r'.+voting\.csv', x), os.listdir(join(pred_dir, d))))
    filenames.sort()

    tmp_acc, tmp_precision, tmp_recall, tmp_f1 = [], [], [], []
    j = 0
    for filename in filenames:
        truth = {}
        for k, v in raw_truth.items():
            temp = k.split('_')
            truth["%s_%s_%s" % (temp[0], temp[1], temp[2])] = v

        len_truth = len(truth)

        csv_path = join(pred_dir, d, filename)
        pred = {}
        i = 0
        with open(csv_path, "r") as filestream:
            for line in filestream:
                i += 1
                currentline = line.rstrip().split(",")
                temp = currentline[0].split('/')[-1].split('_')
                pred["%s_%s_%s" % (temp[0], temp[1], temp[2])] = currentline[1]

        len_pred = len(pred)
        # print('Size of predictions: {}, num of lines is {}'.format(len(pred), i))

        # extract the list of keys
        keys_truth = set(truth.keys())
        keys_pred = set(pred.keys())
        intersection = keys_truth & keys_pred
        # print('Size of intersection: {}'.format(len(intersection)))
        truth = {k: truth.get(k, None) for k in intersection}
        pred = {k: pred.get(k, None) for k in intersection}

        names = list(set().union(truth.values(), pred.values()))

        y_true = list(truth.values())
        y_pred = list(pred.values())

        # Compute confusion matrix
        report_lr = precision_recall_fscore_support(y_true, y_pred, average='macro')
        acc = accuracy_score(y_true, y_pred, normalize=True)
        print(
            "File %s: num_truth= %d, num_pred = %d, intersection = %d, precision = %0.3f, recall = %0.3f, F1 = %0.3f, accuracy = %0.3f\n" % \
            (csv_path.split('/')[-1], len_truth, len_pred, len(intersection), report_lr[0], report_lr[1], report_lr[2],
             acc))
        tmp_acc.append(acc)
        tmp_precision.append(report_lr[0])
        tmp_recall.append(report_lr[1])
        tmp_f1.append(report_lr[2])

        j += 1

        # check converge
        if j > 2:
            window = np.array(tmp_acc[j - 3:j])

            if window.std() < cfg['draw']['hyper']['thres'][cfg['draw']['hyper']['site']]:
                # print('iter {} has std {}'.format(i, window.std()))
                df.loc[len(df)] = [d.split('_')[-1], tmp_precision[-2], tmp_recall[-2], tmp_f1[-2], tmp_acc[-2], j]
                # print('folder {} converge!, df is {}'.format(folder, df))
                break
            elif j == len(filenames):
                print('folder {} does NOT converge!'.format(d))
                df.loc[len(df)] = [d.split('_')[-1], tmp_precision[-1], tmp_recall[-1], tmp_f1[-1], tmp_acc[-1], j]
    print('END OF Dir {} .........'.format(d))


# filter
df = df[~df[' '].isin(cfg['draw']['hyper']['remove'])]
print(df)


if cfg['draw']['hyper']['style'] == 0:

    df = df.drop(['converge_iter.'], axis=1)
    df = df.set_index(' ').T

    # save to fig dir
    color_sink = ['C4', 'C5', 'C6', 'C7', 'C8', 'C9']

    plot = df.plot.bar(alpha=0.75, rot=0, figsize=(23, 12), fontsize=50, width=0.8, color=color_sink,
                       linewidth=4, edgecolor=['black']*df.shape[1])
    plot.set_ylim([0, 1.19])
    plot.set_ylabel("Performance", fontsize=60)
    ncol = df.shape[1]
    print(df.shape)
    l_ft = 52*4/df.shape[1]
    plot.legend(ncol=ncol, loc=9, fontsize=l_ft)
    fig = plot.get_figure()
    fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['hyper']['site'])
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)

    fig_name = cfg['draw']['hyper']['para'] + '_' + cfg['draw']['hyper']['site']
    fig_path = join(fig_folder, fig_name + '.pdf')
    fig.savefig(fig_path, bbox_inches='tight')
    fig_path = join(fig_folder, fig_name + '.png')
    fig.savefig(fig_path, bbox_inches='tight')

else:
    color_sink = [('C2', 'C3'), ('C4', 'C5'), ('C0', 'C1')]

    # plot
    fig, ax1 = plt.subplots(figsize=(23, 10))
    plt.locator_params(axis='x', nticks=5)
    tick_size = 55
    label_size = 75

    lw = 10
    marker_size = 32

    if cfg['draw']['hyper']['para'] == 'fuzzier':
        color_idx = 2
    else:
        color_idx = 1

    print(color_sink[color_idx])
    t = df[' '].tolist()
    ax1.plot(df[' '].tolist(), df['F1 score'].tolist(), color_sink[color_idx][0] + 'o-',
             linewidth=lw, markersize=marker_size)
    # ax1.xaxis.set_major_locator(ticker.MultipleLocator(np.mean(np.diff(t))))
    ax1.set_xlabel(cfg['draw']['hyper']['hyper_map'][cfg['draw']['hyper']['para']], fontsize=label_size)
    x_min, x_max = float(min(df[' '].tolist())), float(max(df[' '].tolist()))
    x_interval = (x_max - x_min)/(len(df[' '].tolist()) -1)
    ax1.set_xticks(np.arange(x_min, x_max+x_interval, x_interval))
    ax1.tick_params('x', labelsize=tick_size)
    # Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylim([0.5, 1])
    ax1.set_ylabel(df.columns[3], color=color_sink[color_idx][0], fontsize=label_size)
    ax1.tick_params('y', colors=color_sink[color_idx][0], labelsize=tick_size)

    ax2 = ax1.twinx()
    ax2.plot(df[' '].tolist(), df['converge_iter.'].tolist(), color_sink[color_idx][1] + 'd-.',
             linewidth=lw, markersize=marker_size)
    ax2.set_ylabel(df.columns[5], color=color_sink[color_idx][1], fontsize=label_size)
    ax2.tick_params('y', colors=color_sink[color_idx][1], labelsize=tick_size)
    ax2.set_ylim([2, 9])

    # save fig
    fig_folder = join(cfg['base_conf']['fig_base'], cfg['draw']['hyper']['site'])
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)

    fig_path = join(fig_folder, cfg['draw']['hyper']['para'] + '_' + cfg['draw']['hyper']['site'] + '.pdf')
    fig.savefig(fig_path, bbox_inches='tight')
    fig_path = join(fig_folder, cfg['draw']['hyper']['para'] + '_' + cfg['draw']['hyper']['site'] + '.png')
    fig.savefig(fig_path, bbox_inches='tight')

    csv_path = join(fig_folder, cfg['draw']['hyper']['para'] + '_' + cfg['draw']['hyper']['site']) + '.csv'
    df.to_csv(csv_path)