# This script trains a one-vs-all svm model to authenticate faces, on top of the features extracted by AutoTune
from sklearn.multiclass import OneVsRestClassifier
import yaml
import decimal
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from os.path import join
import tensorflow as tf
import numpy as np
from itertools import cycle
import facenet
import os
from pandas import DataFrame
from sklearn.preprocessing import label_binarize
import math
import pickle
from sklearn.svm import SVC
from sklearn.metrics import roc_curve, auc
from scipy.optimize import brentq
from scipy.interpolate import interp1d

project_dir = os.path.dirname(os.getcwd())

with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)

if cfg['specs']['set_gpu']:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(cfg['base_conf']['gpu_num'])

batch_size = 1000
nrof_train_images_per_class = 100
min_nrof_images_per_class = 10
image_size = 160
threshold = 0.6

kernel = cfg['svm_inference']['kernel']
model_name = cfg['svm_inference']['model_name']
path_train = cfg['svm_inference']['path_train']
test = cfg['svm_inference']['test']
test_cameras = cfg['svm_inference']['test_cameras']
path_other = cfg['svm_inference']['path_other']
folder = cfg['svm_inference']['folder']
mode = cfg['svm_inference']['mode']

project_dir = os.path.dirname(os.getcwd())


# In[30]:


def embedding(path):
    with tf.Graph().as_default():
        with tf.Session() as sess:
            np.random.seed(seed=2)

            dataset = facenet.get_dataset(path)

            paths, labels = facenet.get_image_paths_and_labels(dataset)
            print(labels)
            print('Number of classes: %d' % len(dataset))
            print('Number of images: %d' % len(paths))

            # Load the model
            print('Loading feature extraction model...')
            model_path = join(project_dir, folder, '%s.pb' % model_name)
            facenet.load_model(model_path)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            embedding_size = embeddings.get_shape()[1]

            # Run forward pass to calculate embeddings
            print('Calculating features for images...')
            nrof_images = len(paths)
            nrof_batches_per_epoch = int(math.ceil(1.0 * nrof_images / batch_size))
            emb_array = np.zeros((nrof_images, embedding_size))

            for i in range(nrof_batches_per_epoch):
                start_index = i * batch_size
                end_index = min((i + 1) * batch_size, nrof_images)
                paths_batch = paths[start_index:end_index]
                images = facenet.load_data(paths_batch, False, False, image_size)
                feed_dict = {images_placeholder: images, phase_train_placeholder: False}
                emb_array[start_index:end_index, :] = sess.run(embeddings, feed_dict=feed_dict)

    return paths, labels, emb_array, dataset

fig_folder = join(cfg['base_conf']['fig_base'], cfg['svm_inference']['site'])
if not os.path.exists(fig_folder):
    os.makedirs(fig_folder)

fpr_dict = dict()
tpr_dict = dict()
df = DataFrame(
        columns=('camera', 'roc_auc', 'eer'))
for test_camera in test_cameras:
    # Get the embedding results of train dataset
    if mode == "train":
        data_dir = join(project_dir, folder, path_train)
        paths, labels, emb_array, dataset = embedding(data_dir)
        class_names = [cls.name.replace('_', ' ') for cls in dataset]
        class_names.append('zzother')

        name_labels = []
        for path in paths:
            name = path.split('/')[-2]
            name_labels.append(name)

        # binarize labels
        labels = label_binarize(name_labels, classes=class_names)

        # Use embedding results to train SVM classifiers
        print('Training classifier...')
        # model = SVC(kernel=kernel, probability=True, random_state=666)
        model = OneVsRestClassifier(SVC(kernel=kernel, random_state=666))
        model.fit(emb_array, labels)

        # save model
        pickle.dump([model, class_names], open(join(project_dir, folder, 'auth_auto_tune.pkl'), 'wb'))

    else:
        with open(join(project_dir, folder, 'auth_auto_tune.pkl'), 'rb') as infile:
            (model, class_names) = pickle.load(infile)

    # Get the embedding results of test dataset
    data_dir = join(test, test_camera)
    paths, labels, emb_array, dataset = embedding(data_dir)

    name_labels = []
    for path in paths:
        name = path.split('/')[-2]
        name_labels.append(name)

    # binarize labels
    labels = label_binarize(name_labels, classes=class_names)
    test_ids = np.nonzero(np.any(labels != 0, axis=0))[0]
    print(test_ids)
    # Compute ROC curve and ROC area for each class
    print('Testing classifier...')
    y_score = model.decision_function(emb_array)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    eer = dict()
    for i in test_ids[:-1]:
        fpr[i], tpr[i], _ = roc_curve(labels[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
        eer[i] = brentq(lambda x: 1. - x - interp1d(fpr[i], tpr[i])(x), 0., 1.)

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(labels[:, :-1].ravel(), y_score[:, :-1].ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    eer["micro"] = brentq(lambda x: 1. - x - interp1d(fpr["micro"], tpr["micro"])(x), 0., 1.)
    fpr_dict[test_camera] = fpr["micro"]
    tpr_dict[test_camera] = tpr["micro"]

    df.loc[len(df)] = [test_camera, roc_auc["micro"], eer["micro"]]

    print('Micro roc_auc: %.3f, Micro EER: %.3f' % (roc_auc["micro"], eer["micro"]))
    print('{} POI: {}'.format(len(class_names), class_names))

    # Plot all ROC curves
    if cfg['svm_inference']['show']:
        lw = 2
        plt.figure()
        plt.plot(fpr["micro"], tpr["micro"],
                 label='micro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["micro"]),
                 color='deeppink', linestyle=':', linewidth=4)

        colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
        for i, color in zip(test_ids[:-1], colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                     label='ROC curve of class {0} (area = {1:0.2f})'
                     ''.format(class_names[i], roc_auc[i]))

        plt.plot([0, 1], [0, 1], 'k--', lw=lw)
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Auth. performance on {} camera, EER={}%'.format(test_camera, eer["micro"]*100))
        plt.legend(loc="lower right")
        plt.show()
        # plt.close('all')
        fig_path = join(fig_folder, test_camera + '_' + cfg['svm_inference']['site'] + '.png')
        plt.savefig(fig_path, bbox_inches='tight')


# plot
lw = 16
linestyle_ls = [':', '--', '-.', '-', '-']
figsize=(22, 20)
ft_size=70
tick_size=80
label_size=90

plt.rcParams.update({'font.size': ft_size})
plt.rcParams.update({'figure.autolayout': True})
plt.rc('xtick', labelsize=tick_size)
plt.rc('ytick', labelsize=tick_size)

plt.figure(figsize=figsize)
# co
j = 0
for test_camera in test_cameras:
    fpr, tpr = fpr_dict[test_camera], tpr_dict[test_camera]
    plt.plot(fpr, tpr, linestyle=linestyle_ls[j],
             lw=lw, label='%s' % cfg['svm_inference']['camera_map'][test_camera])
    j += 1

# plt.plot([0, 1], [0, 1], color='darkorange', lw=2, linestyle='--')
plt.xlim([-0.01, 1.0])
plt.ylim([0.01, 1.05])
plt.xlabel('False Positive Rate', fontsize=label_size)
plt.ylabel('True Positive Rate', fontsize=label_size)
# plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")


fig_path = join(fig_folder, 'cross_auth_' + cfg['svm_inference']['site'] +'.pdf')
plt.savefig(fig_path, bbox_inches='tight')
fig_path = join(fig_folder, 'cross_auth_' + cfg['svm_inference']['site'] +'.png')
plt.savefig(fig_path, bbox_inches='tight')

csv_path = join(fig_folder, 'cross_auth_' + cfg['svm_inference']['site']) + '.csv'
df.to_csv(csv_path)