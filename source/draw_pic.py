import os
import yaml
from os.path import join
import numpy as np
import re
from sklearn.preprocessing import LabelEncoder
import pickle
import tools
import argparse
import sys
import csv
from shutil import copyfile, rmtree
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.style.use('ggplot')
from matplotlib import cm
from numpy import arange


# In[70]:


project_dir = os.path.dirname(os.getcwd())
folder_name = 'middle_data'
end_pk_num = 15
with open(join(project_dir, 'config.yaml'), 'r') as f:
    cfg = yaml.load(f)
middle_data_path = join(project_dir, folder_name)
meeting_npy_paths = {}
start_matrix = tools.get_meeting_people_name(middle_data_path, 55)
for i in range(1, end_pk_num+1):
    meeting_npy_paths[i] = tools.get_meeting_and_path(middle_data_path, r'.+\.pk' + re.escape(str(i)) + r'$')


# In[71]:


start_matrix


# In[72]:


all_peoples = []
for _, v in start_matrix.items():
    all_peoples.extend(v)
all_peoples = list(set(all_peoples))


# In[73]:


# all_peoples.sort(key=lambda x: int(x[1:]))


# In[74]:


len(all_peoples)


# In[75]:


meetings= list(start_matrix.keys())


# In[76]:


meetings.sort()


# In[77]:


meetings


# In[78]:


true_label = {}
with open(join(project_dir, folder_name, 'true_label.pk'), 'rb') as f:
    true_label = pickle.load(f)
true_label
tem_label = {}
for k,v in true_label.items():
    temp = k.split('_')
    tem_label["%s_%s_%s"%(temp[0], temp[1], temp[2])] = v
true_label = tem_label


# In[79]:


true_pics = {}
for sub_path in os.listdir(middle_data_path):
    if sub_path=='true_label.pk':
        continue
    pics = os.listdir(join(middle_data_path, sub_path, 'mtcnn'))
    true_peo = []
    for pic in pics:
        temp = pic.split('_')
        if "%s_%s_%s"%(temp[0], temp[1], temp[2]) in true_label:
            true_peo.append(true_label["%s_%s_%s"%(temp[0], temp[1], temp[2])])
    true_pics[sub_path] = list(set(true_peo))


# In[80]:


def draw_pic(data, pic_name, x_name, y_name):
    x_classes = ['e%d'%x for x in range(data.shape[1])]
    fig, ax = plt.subplots()
    plt.rcParams.update({'font.size': 25})
    # plt.rcParams.update({'figure.autolayout': True})
    fig.set_size_inches(25, 20)
    ax.imshow(data, interpolation='nearest', cmap=cm.YlGn)
    le = LabelEncoder()
    y_classes = le.fit_transform(y_name)
    tick_marks_y = arange(len(y_classes))
    plt.yticks(tick_marks_y, y_classes)
    tick_marks_x = arange(len(x_classes))
    plt.xticks(tick_marks_x, x_classes)
    plt.xticks(tick_marks_x, x_classes, rotation=45)
    plt.ylabel('Subject ID')
    plt.xlabel('Event index')
    fig_folder = '../../../fig/wifi_cm'
    if not os.path.exists(fig_folder):
        os.makedirs(fig_folder)
    plt.savefig('../../../fig/wifi_cm/%s.pdf'%pic_name, bbox_inches='tight')


# In[81]:


def draw_img_with_list(peoples, meetings, data, pic_name, x_name):
    matirx = np.zeros((len(peoples), len(meetings))).astype(np.float64)
    for w in range(len(meetings)):
        for h in range(len(peoples)):
            if peoples[h] in data[meetings[w]]:
                matirx[h, w] = 1
    draw_pic(matirx, pic_name, x_name, peoples)
#     x_classes = ['e%d'%x for x in range(len(meetings))]
#     y_classes = peoples
#     fig, ax = plt.subplots()
#     fig.set_size_inches(25, 20)
#     ax.imshow(matirx, interpolation='nearest', cmap=cm.Blues)
#     tick_marks_y = arange(len(y_classes))
#     plt.yticks(tick_marks_y, y_classes)
#     tick_marks_x = arange(len(x_classes))
#     plt.xticks(tick_marks_x, x_classes)
#     plt.ylabel('People')
#     plt.xlabel(x_name)
#     plt.savefig('%s.png'%pic_name)


# In[82]:


draw_img_with_list(all_peoples, meetings, true_pics, 'true', 'True')


# In[83]:


draw_img_with_list(all_peoples, meetings, start_matrix, 'start', 'Start')

draw_img_with_list(all_peoples, meetings, true_pics, 'true', 'True')
# In[84]:


def draw_img_with_dic(peoples, meetings, data, pic_name, x_name):
    matirx = np.zeros((len(peoples), len(meetings))).astype(np.float64)
    for w in range(len(meetings)):
        for h in range(len(peoples)):
            if peoples[h] in data[meetings[w]]:
                matirx[h, w] = data[meetings[w]][peoples[h]]
    draw_pic(matirx, pic_name, x_name, peoples)


# In[85]:


for cyc_num, pk_list in meeting_npy_paths.items():
    draw_data = {}
    for meet, paths in pk_list.items():
        with open(paths, 'rb') as f:
            draw_data[meet] = pickle.load(f)
    draw_img_with_dic(all_peoples, meetings, draw_data, 'cycle_%d'%cyc_num, 'Cycle %d'%cyc_num)


# In[ ]:




